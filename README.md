# Sites Pour Poster Ton Projet
Une liste de ressources en français pour présenter ton nouveau projet personnel auto-financé ou ta startup qui démarre.

Inspiré de [PlacesToPostYourStartup](https://github.com/mmccaff/PlacesToPostYourStartup/), excellente liste de ressources anglophones.

* Human Coders - https://news.humancoders.com/t/projets
* J'aime les startups  - http://www.jaimelesstartups.fr/ajouter-une-startup/
* Pragmatic Entrepreneurs Feedback - https://forum.pragmaticentrepreneurs.com/c/feedback
* /r/projetperso - https://www.reddit.com/r/projetperso/
* 1001startups - http://1001startups.fr/proposer-startup/

## Licence
[![CC BY-SA 2.0 FR](https://i.creativecommons.org/l/by-sa/2.0/88x31.png)](https://creativecommons.org/licenses/by-sa/2.0/fr/)

## Éditeur
Carl Chenet - [@carl_chenet](https://twitter.com/carl_chenet)

## Contributeurs
* Camille Roux - [@CamilleRoux](https://twitter.com/CamilleRoux)
* Youri Galescot - [@ygalescot](https://twitter.com/ygalescot)
